/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/actions/eat.js":
/*!************************************!*\
  !*** ./application/actions/eat.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar eat = 'кушает';\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (eat);\n\n//# sourceURL=webpack:///./application/actions/eat.js?");

/***/ }),

/***/ "./application/actions/run.js":
/*!************************************!*\
  !*** ./application/actions/run.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar run = 'бегает';\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (run);\n\n//# sourceURL=webpack:///./application/actions/run.js?");

/***/ }),

/***/ "./application/actions/sweep.js":
/*!**************************************!*\
  !*** ./application/actions/sweep.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar sweep = 'несется';\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (sweep);\n\n//# sourceURL=webpack:///./application/actions/sweep.js?");

/***/ }),

/***/ "./application/actions/transferMail.js":
/*!*********************************************!*\
  !*** ./application/actions/transferMail.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nvar transferMail = 'приносит почту';\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (transferMail);\n\n//# sourceURL=webpack:///./application/actions/transferMail.js?");

/***/ }),

/***/ "./application/birds/chicken.js":
/*!**************************************!*\
  !*** ./application/birds/chicken.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _actions_sweep_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../actions/sweep.js */ \"./application/actions/sweep.js\");\n/* harmony import */ var _actions_eat_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../actions/eat.js */ \"./application/actions/eat.js\");\n/* harmony import */ var _actions_run_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../actions/run.js */ \"./application/actions/run.js\");\n\r\n\r\n\r\nfunction Chicken(name) {\r\n    this.name = name;\r\n    this.sweep = _actions_sweep_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\r\n    this.eat = _actions_eat_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\r\n    this.run = _actions_run_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"];\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Chicken);\n\n//# sourceURL=webpack:///./application/birds/chicken.js?");

/***/ }),

/***/ "./application/birds/pigeon.js":
/*!*************************************!*\
  !*** ./application/birds/pigeon.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _actions_eat_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../actions/eat.js */ \"./application/actions/eat.js\");\n/* harmony import */ var _actions_run_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../actions/run.js */ \"./application/actions/run.js\");\n/* harmony import */ var _actions_transferMail_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../actions/transferMail.js */ \"./application/actions/transferMail.js\");\n\r\n\r\n\r\nfunction Pigeon(name) {\r\n    this.name = name;\r\n    this.eat = _actions_eat_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\r\n    this.run = _actions_run_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\r\n    this.transferMail = _actions_transferMail_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"];\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Pigeon);\n\n//# sourceURL=webpack:///./application/birds/pigeon.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _birds_chicken_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./birds/chicken.js */ \"./application/birds/chicken.js\");\n/* harmony import */ var _birds_pigeon_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./birds/pigeon.js */ \"./application/birds/pigeon.js\");\n\n\n\nvar myChicken = new _birds_chicken_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"](\"Иван\");\nvar chicken = document.createElement('div');\nvar chickenText = 'Курица ' + myChicken.name + ' ' + myChicken.sweep + ', ' + myChicken.eat + ', ' + myChicken.run;\nchicken.innerText = chickenText;\ndocument.body.appendChild(chicken);\n\nvar myPigeon = new _birds_pigeon_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"](\"Саня\");\nvar pigeon = document.createElement('div');\nvar pigeonText = 'Голубь ' + myPigeon.name + ' ' + myPigeon.eat + ', ' + myChicken.run + ', ' + myPigeon.transferMail;\npigeon.innerText = pigeonText;\ndocument.body.appendChild(pigeon);\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });