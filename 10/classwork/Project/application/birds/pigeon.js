import eat from './../actions/eat.js';
import run from './../actions/run.js';
import transferMail from './../actions/transferMail.js';
function Pigeon(name) {
    this.name = name;
    this.eat = eat;
    this.run = run;
    this.transferMail = transferMail;
}

export default Pigeon;