import sweep from './../actions/sweep.js';
import eat from './../actions/eat.js';
import run from './../actions/run.js';
function Chicken(name) {
    this.name = name;
    this.sweep = sweep;
    this.eat = eat;
    this.run = run;
}

export default Chicken;