import Chicken from './birds/chicken.js'
import Pigeon from './birds/pigeon.js'

var myChicken = new Chicken("Иван");
var chicken = document.createElement('div');
var chickenText = 'Курица ' + myChicken.name + ' ' + myChicken.sweep + ', ' + myChicken.eat + ', ' + myChicken.run;
chicken.innerText = chickenText;
document.body.appendChild(chicken);

var myPigeon = new Pigeon("Саня");
var pigeon = document.createElement('div');
var pigeonText = 'Голубь ' + myPigeon.name + ' ' + myPigeon.eat + ', ' + myChicken.run + ', ' + myPigeon.transferMail;
pigeon.innerText = pigeonText;
document.body.appendChild(pigeon);