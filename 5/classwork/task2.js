/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
    1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
    1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();

*/

// 1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
function ChangeColor (color) {
    var title = document.createElement('h1');
        title.innerText = 'I know how binding works in JS';
    document.body.appendChild(title)
    document.body.style.color = color;
    document.body.style.background = this.background;
}

ChangeColor.call({
    background: 'blue'
}, 'white');

// 1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
var title = {
    background: 'red',
    color: 'yellow',
};

function bindColor() {
    document.body.style.color = this.color;
    document.body.style.background = this.background;
}

var x = bindColor.bind(title);
    x();

// 1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();
function ChangeTitle(titleText) {
    var title = document.querySelector('h1');
        title.innerText = this.text;
}

var titleText = ['New Title'];

ChangeTitle.apply({
    text: titleText
}, titleText);

