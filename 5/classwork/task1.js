/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var Train = {
    name: 'Hyunday',
    speed: '100',
    passengers: 500,
    go: function(){
        console.log('Поезд ' + this.name + ' везет ' + this.passengers + ' пассажиров со скоростью ' + this.speed + ' км/ч');
    },
    stop: function() {
        this.speed = 0;
        console.log('Поезд ' + this.name + ' остановился. ' + 'Скорость ' + this.speed + ' км/ч');
    },
    pick: function(x) {
        this.passengers += x;
        console.log('Количество пассажиров: ' + this.passengers);
    },
};

Train.go();
Train.stop();
Train.pick(50);