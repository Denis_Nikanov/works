/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

function Dog(name, breed) {
    this.name = name;
    this.breed = breed;
    this.run = function () {
        console.log("Собака по кличке " + this.name + " породы " + this.breed + " бежит");
    }
    this.eat = function () {
        console.log("Собака по кличке " + this.name + " породы " + this.breed + " кушает");
    }
}

var myDog = new Dog('Джина', "Пикинес");

function addDogAction() {
    myDog.run();
    myDog.eat();
}

function viewDogProps() {
    var key;
    for (key in myDog) {
        console.log('Свойство объекта myDog:', key);
    }
    addDogAction();
}

viewDogProps();

