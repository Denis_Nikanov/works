/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

// Получение  случайного целого числа между минимальным и максимальным значением
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Получение случайного цвета по RGB
function getRandomRgbColor() {
    var r = getRandomIntInclusive(0, 256);
    var g = getRandomIntInclusive(0, 256);
    var b = getRandomIntInclusive(0, 256);
    var colorCodeRgb = "rgb(" + r + ", " + g + ", " + b + ")";

    console.log(colorCodeRgb);
    document.body.style.background = colorCodeRgb;
}

// Получение случайного цвета по HEX
function getRandomHexColor() {
    var rr = getRandomIntInclusive(17, 99).toString(16);
    var gg = getRandomIntInclusive(17, 99).toString(16);;
    var bb = getRandomIntInclusive(17, 99).toString(16);
    var colorCodeHex = "#" + rr + gg + bb;

    return colorCodeHex;
}

// Вывод кода цвета по центру страницы
var btn = document.getElementById('btn');
    btn.style.display = 'block';
    btn.style.margin = '0 auto';
var colorCode = document.createElement('div');
    colorCode.innerText = 'HEX: ' + getRandomHexColor();
var colorCodeContainer = document.createElement('div');
    colorCodeContainer.className = 'color-center';
    colorCodeContainer.style.position = 'absolute';
    colorCodeContainer.style.top = '50%';
    colorCodeContainer.style.right = '50%';
    colorCodeContainer.style.fontSize = '50px';
    colorCodeContainer.style.fontWeight = '600';
    colorCodeContainer.style.textAlign = 'center';
    colorCodeContainer.style.transform = 'translate(50%, -50%)';
    colorCodeContainer.style.color = '#fff';
    colorCodeContainer.appendChild(colorCode);
    colorCodeContainer.appendChild(btn);
var wrap = document.getElementById('app');
    wrap.appendChild(colorCodeContainer);

// Смена цвета фона при перезагрузке страницы
window.onload = ChangeColor();

// Смена цвета по нажатию на кнопку
function ChangeColor(){
    document.body.style.background = getRandomHexColor();
    colorCode.innerText = 'HEX: ' + getRandomHexColor();
}