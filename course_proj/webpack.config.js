var webpack = require('webpack');
var Encore = require('@symfony/webpack-encore');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

Encore
// the project directory where all compiled assets will be stored
    .setOutputPath('public')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/')

    // will compile files
    .addEntry('index', './src/js/index.js')

    .enablePostCssLoader()

    // allow sass/scss files to be processed
    .enableSassLoader()

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    .createSharedEntry('vendors', [
        './src/js/itea-carousel-slider/itea-carousel-slider.js',

        // you can also extract CSS - this will create a 'vendor.css' file
        './src/scss/style.scss',
        './src/css/fontello.css',
        './src/css/animate.css'
    ])

    // path to compiled files
    .configureFilenames({
            js: 'js/[name].[chunkhash].js',
            css: 'css/[name].[contenthash].css',
            images: 'img/[name].[hash:8].[ext]',
            fonts: 'fonts/[name].[ext]'
     })

    // add custom module
    .addLoader({
        test: /\.pug$/,
        loader: 'pug-loader',
        options: {
            pretty: true
        }
    })

    .addPlugin(
        new CopyWebpackPlugin([
            {
                from: './src/img/',
                to: './img/'
            },
            {
                from: './src/fonts/',
                to: './fonts/'
            }
        ])
    )

    .addPlugin(
        new HtmlWebpackPlugin(
            {
                filename: 'index.html',
                chunks: ['manifest', 'vendors', 'index'],
                chunksSortMode: 'manual',
                template: 'src/pug/index.pug'
            }
        )
    )
;

var config  = Encore.getWebpackConfig();

module.exports = config;