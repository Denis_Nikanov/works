let sliderStep = 0,
    nextSlideIndex = 0;

function prev() {
    for (var i = 0; i < this.item.length; i++) {
        let sliderItem = this.item[i],
            sliderItemWidth = sliderItem.offsetWidth,
            curTransform = window.getComputedStyle(this.feed).transform,
            curTransformX = curTransform.replace(/[^0-9\-.,]/g, '').split(',')[4];

        this.nextBtn.classList.remove('ics__nav-btn--disabled');
        if (curTransformX == -sliderItemWidth) {
            this.prevBtn.classList.add('ics__nav-btn--disabled');
        }
        sliderStep = parseFloat(curTransformX) + parseFloat(sliderItemWidth);
        this.feed.style.transform = "translate3d(" + sliderStep + "px, 0, 0)";
    }

    let prevActiveCarousel = document.querySelectorAll('.ics--active-carousel');
    for (var i = 0; i < prevActiveCarousel.length; i++) {
        prevActiveCarousel[i].classList.remove('ics--active-carousel');
    }
    let carousel = this.carousel;
    carousel.classList.add('ics--active-carousel');

    let sliderItem = this.carousel.getElementsByClassName('ics__item'),
        slidesCount = sliderItem.length,
        sliderAnimatedItemIndex = slidesCount - (nextSlideIndex - 1),
        sliderAnimatedClass = this.animation,
        sliderItemAnimated = this.carousel.querySelector('.ics__item:nth-last-child(' + sliderAnimatedItemIndex + ')');
    sliderItemAnimated.classList.add('animated');
    sliderItemAnimated.classList.add(this.animation);
    setTimeout(function() {
        sliderItemAnimated.classList.remove(sliderAnimatedClass)
    }, 400);
    nextSlideIndex -= 1;
}

function next() {
    let sliderItem = this.carousel.getElementsByClassName('ics__item'),
        slidesCount = sliderItem.length;

    let prevActiveCarousel = document.querySelectorAll('.ics--active-carousel');
    for (var i = 0; i < prevActiveCarousel.length; i++) {
        prevActiveCarousel[i].classList.remove('ics--active-carousel');
        if (('#' + this.carousel.id + '') != ('#' + prevActiveCarousel[i].id + '')) {
            nextSlideIndex = 0;
        }
    }
    let carousel = this.carousel;
    carousel.classList.add('ics--active-carousel');

    let sliderAnimatedItemIndex = slidesCount - this.slides - nextSlideIndex,
        sliderAnimatedClass = this.animation,
        sliderItemAnimated = this.carousel.querySelector('.ics__item:nth-last-child(' + sliderAnimatedItemIndex + ')');
    sliderItemAnimated.classList.add('animated');
    sliderItemAnimated.classList.add(sliderAnimatedClass);
    setTimeout(function() {
        sliderItemAnimated.classList.remove(sliderAnimatedClass)
    }, 700);

    nextSlideIndex += 1;

    for (var i = 0; i < this.item.length; i++) {
        let sliderItem = this.item[i],
            sliderItemWidth = sliderItem.offsetWidth,
            curTransform = window.getComputedStyle(this.feed).transform,
            curTransformX = curTransform.replace(/[^0-9\-.,]/g, '').split(',')[4],
            slidesHidden = slidesCount - this.slides,
            transformMaxVal = -(slidesHidden * sliderItemWidth - sliderItemWidth),
            sliderStep = curTransformX - sliderItemWidth;
        if (sliderStep <= 0) {
            this.prevBtn.classList.remove('ics__nav-btn--disabled');
        }
        if (curTransformX == transformMaxVal) {
            this.nextBtn.classList.add('ics__nav-btn--disabled');
        }
        this.feed.style.transform = "translate3d(" + sliderStep + "px, 0, 0)";
    }
}

export {prev, next};