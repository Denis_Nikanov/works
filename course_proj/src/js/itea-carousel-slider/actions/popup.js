let popupImgContainer = document.querySelector('.popup__img'),
    popupCloseBtn = document.querySelector('.popup__close'),
    popupOverlay = document.querySelector('.popup__overlay'),
    popup = document.querySelector('.popup');

function setPopup() {
    let popupImg = this.outerHTML;

    popupImgContainer.innerHTML = popupImg;
    popupCloseBtn.addEventListener('click', closePopup.bind(this));
    popupOverlay.addEventListener('click', closePopup.bind(this));
    popup.style.display = 'block';
}

function closePopup() {
    popup.style.display = 'none';
}

export default setPopup;