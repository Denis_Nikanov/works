import {prev, next} from './actions/navigation.js';
import setPopup from './actions/popup.js';

class Carousel {
    constructor(settings) {
        this.carousel = document.querySelector(settings.carousel) || document.querySelector('.ics');
        this.container = this.carousel.querySelector('.ics__container');
        this.feed = this.carousel.querySelector('.ics__feed');
        this.item = this.carousel.querySelectorAll('.ics__item');
        this.image = this.carousel.querySelectorAll('.ics__item img');
        this.slides = settings.slides || 4;
        this.prevBtn = this.carousel.querySelector(settings.prev) || this.carousel.querySelector('.ics__nav-btn--prev');
        this.nextBtn = this.carousel.querySelector(settings.next) || this.carousel.querySelector('.ics__nav-btn--next');
        this.speed = settings.speed || 200;
        this.animation = settings.animation || 'zoomIn';
        this.prev = prev;
        this.next = next;
        this.popup = setPopup;

        for (var i = 0; i < this.item.length; i++) {
            let sliderItem = this.item[i];
            sliderItem.style.width = this.container.offsetWidth / this.slides + 'px';
        }
        for (var i = 0; i < this.image.length; i++) {
            this.image[i].addEventListener('click', this.popup);
        }
        this.feed.style.transition = this.speed + 'ms';
        this.nextBtn.addEventListener('click', this.next.bind(this));
        this.prevBtn.addEventListener('click', this.prev.bind(this));
        this.prevBtn.classList.add('ics__nav-btn--disabled');
    }
}

export default Carousel;