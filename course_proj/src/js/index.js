import Carousel from './itea-carousel-slider/itea-carousel-slider.js';

let carSlider = new Carousel({
    carousel: '#ics--car',
    slides: 5,
    speed: 400
});

let pizzaSlider = new Carousel({
    carousel: '#ics--pizza',
    slides: 4,
    speed: 300,
    animation: 'flipInX'
});

let dogSlider = new Carousel({
    carousel: '#ics--dog',
    slides: 3,
    animation: 'bounceIn'
});