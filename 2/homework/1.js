
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

var buttonContainer = document.getElementById('buttonContainer'),
    buttons = buttonContainer.querySelectorAll('.showButton');
    buttons.forEach(function(button) {
        button.onclick = function(e) {
            // hideAllTabs();
            var activeBtns = buttonContainer.querySelectorAll('.active');
                activeBtns.forEach(function(activeBtn) {
                    activeBtn.classList.remove('active');
                });

            e.target.classList.add('active');

            var activeButton = document.querySelector('.showButton.active'),
                activeBtnId = activeButton.dataset.tab;

            hideAllTabs();

            var activeTab = tabContainer.querySelector('.tab[data-tab="'+ activeBtnId +'"]');
                activeTab.classList.add('active');
        }

    });

function hideAllTabs() {
    var tabContainer = document.getElementById('tabContainer'),
        tabs = tabContainer.querySelectorAll('.tab');
    tabs.forEach(function(tab){
        tab.classList.remove('active');
    });
}


